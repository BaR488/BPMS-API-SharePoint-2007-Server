﻿using System;
using System.Collections.Generic;
using BPMSAPI.Server.SharePoint2007.Models;
using FlexberryBPMIntegrationService;

namespace BPMSAPI.Server.SharePoint2007.Helpers
{
    public class WorkflowInstanceHelper
    {
        public static WorkflowInstanceState ParseState(string state)
        {
            WorkflowInstanceState.WorkflowInstanceStateValueEnum stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.CreatedEnum;
            switch (state)
            {
                case "NotStarted":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.CreatedEnum;
                    break;
                case "Running":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.RunningEnum;
                    break;
                case "Finished":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.FinishedEnum;
                    break;
                case "Cancelled":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.CancelledEnum;
                    break;
                case "Faulted":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.FaultedEnum;
                    break;
                case "Restarted":
                    stateEnum = WorkflowInstanceState.WorkflowInstanceStateValueEnum.RunningEnum;
                    break;
            }

             return new WorkflowInstanceState
            {
                WorkflowInstanceStateValue = stateEnum
            };
        }

        public static Models.WorkflowInstance WorkflowInstanceParse(FlexberryBPMIntegrationService.WorkflowInstance workflowInstance)
        {


            return new Models.WorkflowInstance
            {
                Id = workflowInstance.Id.ToString(),
                WorkflowId = workflowInstance.WorkflowId.ToString(),
                State = ParseState(workflowInstance.State)
            };
        }

        internal static Models.WorkflowInstanceFull WorkflowInstanceFullParse(FlexberryBPMIntegrationService.WorkflowInstance workflowInstance)
        {
            return new Models.WorkflowInstanceFull
            {
                Id = workflowInstance.Id.ToString(),
                WorkflowId = workflowInstance.WorkflowId.ToString(),
                State = ParseState(workflowInstance.State)
            };
        }

        internal static WorkflowInstanceVariable WorkflowInstanceVariablesParse(WorkflowInstanceParam workflowInstanceParam)
        {
            return new WorkflowInstanceVariable
            {
                Id = workflowInstanceParam.Id.ToString(),
                Value = workflowInstanceParam.Value
            };
        }
    }
}
