﻿using FlexberryBPMIntegrationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BPMSAPI.Server.SharePoint2007.Helpers
{
    public class WorkflowHelper
    {
        public static Models.Workflow WorkflowParse(WorkflowsView workflow)
        {
            return new Models.Workflow
            {
                Id = workflow.Id.ToString(),
                Version = workflow.Version.ToString(),
                Name = workflow.Name
            };
        }
    }
}
