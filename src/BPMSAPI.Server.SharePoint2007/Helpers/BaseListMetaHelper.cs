﻿using BPMSAPI.Server.SharePoint2007.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BPMSAPI.Server.SharePoint2007.Helpers
{
    public class BaseListMetaHelper
    {
        public static BaseListMeta GetBaseListMeta(int? pageNum, int? pageSize, int? totalRecordCount)
        {
            var meta = new BaseListMeta();
            meta.PageNum = pageNum ?? 1;
            meta.PageSize = pageSize ?? totalRecordCount;
            meta.TotalRecordCount = totalRecordCount;
            meta.TotalPages = pageSize != null ? meta.TotalRecordCount / pageSize + (meta.TotalRecordCount % pageSize > 0 ? 1 : 0) : 1;
            return meta;
        }
    }
}
