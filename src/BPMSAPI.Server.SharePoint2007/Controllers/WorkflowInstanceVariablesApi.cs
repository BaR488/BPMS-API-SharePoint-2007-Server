/*
 * BPMS API
 *
 * Универсальный интерфейс взаимодействия с различными BPM системами.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using BPMSAPI.Server.SharePoint2007.Attributes;
using BPMSAPI.Server.SharePoint2007.Models;
using FlexberryBPMIntegrationService;
using static FlexberryBPMIntegrationService.FlexberryBPMIntegrationServiceSoapClient;
using BPMSAPI.Server.SharePoint2007.Helpers;

namespace BPMSAPI.Server.SharePoint2007.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowInstanceVariablesApiController : Controller
    { 
        /// <summary>
        /// Добавление переменной в экземпляр процесса.
        /// </summary>
        
        /// <param name="workflowId">Идентификатор процесса</param>
        /// <param name="instanceId">Идентификатор экземпляра процесса</param>
        /// <param name="workflowInstanceVariable">Переменная процесса</param>
        /// <response code="201">Переменная добавлена в процесс</response>
        /// <response code="409">Переменная уже существует</response>
        [HttpPost]
        [Route("/api/workflows/{workflowId}/instances/{instanceId}/variables")]
        [ValidateModelState]
        [SwaggerOperation("AddEngineWorkflowInstanceVariable")]
        public virtual IActionResult AddEngineWorkflowInstanceVariable([FromRoute][Required]string workflowId, [FromRoute][Required]string instanceId, [FromBody]WorkflowInstanceVariable workflowInstanceVariable)
        { 
            return StatusCode(501);
        }

        /// <summary>
        /// Получение значения переменной экземпляра процесса.
        /// </summary>
        /// <param name="workflowId">Идентификатор процесса</param>
        /// <param name="instanceId">Идентификатор экземпляра процесса</param>
        /// <param name="variableId">Идентификатор переменной экземпляра процесса</param>
        /// <response code="200">Значение переменной экземпляра процесса.</response>
        [HttpGet]
        [Route("/api/workflows/{workflowId}/instances/{instanceId}/variables/{variableId}")]
        [ValidateModelState]
        [SwaggerOperation("GetEngineWorkflowInstanceVariableValue")]
        [SwaggerResponse(statusCode: 200, type: typeof(WorkflowInstanceVariable), description: "Значение переменной экземпляра процесса.")]
        public virtual async Task<IActionResult> GetEngineWorkflowInstanceVariableValue(
            [FromRoute] [Required] string workflowId, [FromRoute] [Required] string instanceId,
            [FromRoute] [Required] string variableId)
        { 
            try
            {
                var flexberryBPMIntegrationServiceSoapClient = new FlexberryBPMIntegrationServiceSoapClient(EndpointConfiguration.FlexberryBPMIntegrationServiceSoap);

                var workflowInstanceVariable = (await flexberryBPMIntegrationServiceSoapClient.GetEngineWorkflowInstanceVariableAsync(workflowId, instanceId, variableId));

                if (workflowInstanceVariable != null )
                {
                    return StatusCode(200, WorkflowInstanceHelper.WorkflowInstanceVariablesParse(workflowInstanceVariable));
                } else
                {
                    return StatusCode(204);
                }                
            }
            catch (Exception e)
            {
                return StatusCode(400, new Error
                {
                    Message = e.Message,
                    Type = Error.TypeEnum.EngineErrorEnum
                });
            }
        }

        /// <summary>
        /// Получение списка переменных экземпляра процесса.
        /// </summary>
        /// <param name="workflowId">Идентификатор процесса</param>
        /// <param name="instanceId">Идентификатор экземпляра процесса</param>
        /// <param name="pageNum">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <response code="200">Список переменных экземпляра процесса.</response>
        [HttpGet]
        [Route("/api/workflows/{workflowId}/instances/{instanceId}/variables")]
        [ValidateModelState]
        [SwaggerOperation("GetEngineWorkflowInstanceVariables")]
        [SwaggerResponse(statusCode: 200, type: typeof(WorkflowInstanceVariableList), description: "Список переменных экземпляра процесса.")]
        public virtual async Task<IActionResult> GetEngineWorkflowInstanceVariablesAsync(
            [FromRoute] [Required] string workflowId, [FromRoute] [Required] string instanceId,
            [FromQuery] int? pageNum, [FromQuery] int? pageSize)
        { 
            try
            {
                var flexberryBPMIntegrationServiceSoapClient = new FlexberryBPMIntegrationServiceSoapClient(EndpointConfiguration.FlexberryBPMIntegrationServiceSoap);

                var meta = BaseListMetaHelper.GetBaseListMeta(
                    pageNum,
                    pageSize,
                    (await flexberryBPMIntegrationServiceSoapClient.GetEngineWorkflowInstanceVariablesTotalCountAsync(workflowId, instanceId)));

                var workflowInstanceVariables = (await flexberryBPMIntegrationServiceSoapClient.GetEngineWorkflowInstanceVariablesAsync(workflowId, instanceId, meta.PageNum, meta.PageSize)).GetEngineWorkflowInstanceVariablesResult;

                var items = workflowInstanceVariables.Select(WorkflowInstanceHelper.WorkflowInstanceVariablesParse).ToList();

                return StatusCode(200, new WorkflowInstanceVariableList
                {
                    Items = items,
                    Meta = meta
                });
            }
            catch (Exception e)
            {
                return StatusCode(400, new Error
                {
                    Message = e.Message,
                    Type = Error.TypeEnum.EngineErrorEnum
                });
            }
        }

        /// <summary>
        /// Изменение переменной в экземпляре процесса.
        /// </summary>
        
        /// <param name="workflowId">Идентификатор процесса</param>
        /// <param name="instanceId">Идентификатор экземпляра процесса</param>
        /// <param name="variableId">Идентификатор переменной экземпляра процесса</param>
        /// <param name="workflowInstanceValiableValue">Значение переменной процесса</param>
        /// <response code="200">Переменная добавлена в процесс</response>
        /// <response code="204">Переменная не существует</response>
        [HttpPut]
        [Route("/api/workflows/{workflowId}/instances/{instanceId}/variables/{variableId}")]
        [ValidateModelState]
        [SwaggerOperation("SetEngineWorkflowInstanceVariable")]
        public virtual IActionResult SetEngineWorkflowInstanceVariable([FromRoute][Required]string workflowId, [FromRoute][Required]string instanceId, [FromRoute][Required]string variableId, [FromBody]string workflowInstanceValiableValue)
        { 
            return StatusCode(501);
        }
    }
}
