/*
 * BPMS API
 *
 * Универсальный интерфейс взаимодействия с различными BPM системами.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BPMSAPI.Server.SharePoint2007.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class BaseListMeta : IEquatable<BaseListMeta>
    { 
        /// <summary>
        /// Gets or Sets PageNum
        /// </summary>
        [Required]
        [DataMember(Name="pageNum")]
        public int? PageNum { get; set; }

        /// <summary>
        /// Gets or Sets PageSize
        /// </summary>
        [DataMember(Name="pageSize")]
        public int? PageSize { get; set; }

        /// <summary>
        /// Gets or Sets TotalPages
        /// </summary>
        [Required]
        [DataMember(Name="totalPages")]
        public int? TotalPages { get; set; }

        /// <summary>
        /// Gets or Sets TotalRecordCount
        /// </summary>
        [Required]
        [DataMember(Name="totalRecordCount")]
        public int? TotalRecordCount { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class BaseListMeta {\n");
            sb.Append("  PageNum: ").Append(PageNum).Append("\n");
            sb.Append("  PageSize: ").Append(PageSize).Append("\n");
            sb.Append("  TotalPages: ").Append(TotalPages).Append("\n");
            sb.Append("  TotalRecordCount: ").Append(TotalRecordCount).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((BaseListMeta)obj);
        }

        /// <summary>
        /// Returns true if BaseListMeta instances are equal
        /// </summary>
        /// <param name="other">Instance of BaseListMeta to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(BaseListMeta other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    PageNum == other.PageNum ||
                    PageNum != null &&
                    PageNum.Equals(other.PageNum)
                ) && 
                (
                    PageSize == other.PageSize ||
                    PageSize != null &&
                    PageSize.Equals(other.PageSize)
                ) && 
                (
                    TotalPages == other.TotalPages ||
                    TotalPages != null &&
                    TotalPages.Equals(other.TotalPages)
                ) && 
                (
                    TotalRecordCount == other.TotalRecordCount ||
                    TotalRecordCount != null &&
                    TotalRecordCount.Equals(other.TotalRecordCount)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (PageNum != null)
                    hashCode = hashCode * 59 + PageNum.GetHashCode();
                    if (PageSize != null)
                    hashCode = hashCode * 59 + PageSize.GetHashCode();
                    if (TotalPages != null)
                    hashCode = hashCode * 59 + TotalPages.GetHashCode();
                    if (TotalRecordCount != null)
                    hashCode = hashCode * 59 + TotalRecordCount.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(BaseListMeta left, BaseListMeta right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BaseListMeta left, BaseListMeta right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
