# BPMSAPI.Server.SharePoint2007 - ASP.NET Core 2.0 Server

Универсальный интерфейс взаимодействия с различными BPM системами.

## Run

Linux/OS X:

```
sh build.sh
```

Windows:

```
build.bat
```

## Run in Docker

```
cd src/BPMSAPI.Server.SharePoint2007
docker build -t bpmsapi.server.sharepoint2007 .
docker run -p 5000:5000 bpmsapi.server.sharepoint2007
```
